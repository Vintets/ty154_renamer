#!/usr/bin/env python
# -*- coding: utf-8 -*-

# for python 3.8.3

if 'import':
    import os
    import sys
    import time
    import glob
    import pprint
    import shutil
    import accessory.colorprint as cp
    import accessory.clear_consol as cc
    import accessory.authorship as auth_sh
    from googletrans import Translator


FOLDER_IN = '3'
FILENAMES = '*.mp3'

FOLDER_OUT = '4'
CLOSECONSOLE = True

def main():
    path_desktop = os.path.join(os.environ['USERPROFILE'], 'Desktop')
    files_in = read_filename(path_desktop)
    if not files_in:
        cp.cprint(f'4В папке не найдены файлы по шаблону ^13_{FOLDER_IN}')
    # print(files_in)

    folder_out = create_folder_out(path_desktop)
    print(f'Выходная директория: {folder_out}\n')

    newnames = list(map(cleaning_name, files_in))
    names_en = list(map(only_name, newnames))
    names_ru = translate_list(names_en)
    # names_ru = list(range(10))
    # print(newnames)
    # print(names_en)
    # print(names_ru)

    uot_num = 1
    for file, name_en, name_ru in zip(files_in, names_en, names_ru):
        folder_num = os.path.join(folder_out, str(uot_num))
        if not os.path.isdir(folder_num): os.makedirs(folder_num)

        file_out = os.path.join(folder_num, f'Nightcore - {name_en}.mp3')
        shutil.move(file, file_out)
        uot_num += 1

        file_out_ru = os.path.join(folder_num, f'{name_ru}.txt')
        with open(file_out_ru, 'w', encoding='utf-8') as f:
            f.write(name_ru)

    save_all_translates(folder_out, names_ru)
    cp.cprint('2Обработка успешно завершена!')

def save_all_translates(folder_out, names_ru):
    file_all_translates = os.path.join(folder_out, 'Имена на русском.txt')
    with open(file_all_translates, 'w', encoding='utf-8') as f:
        f.write('\n'.join(names_ru))

def create_folder_out(path_desktop):
    folder_out_full = os.path.join(path_desktop, FOLDER_OUT)
    if not os.path.isdir(folder_out_full): os.makedirs(folder_out_full)
    return folder_out_full

def read_filename(path_desktop):
    foldername_in = os.path.join(path_desktop, FOLDER_IN)
    print(f'Входная директория:  {foldername_in}')
    files = glob.glob(os.path.join(foldername_in, FILENAMES))
    return files

def cleaning_name(file):
    name = os.path.basename(file)
    newname = name
    if name.startswith('ES_'):
        newname = name[3:]
    return newname

def only_name(name):
    if name.endswith('.mp3'):
        name = name[:-4]
    return name

def translate_(translator, name_eng):
    result = translator.translate(name_eng, src='en', dest='ru')
    return result.text

def translate_list(names_en):
    translator = Translator()
    result = translator.translate(names_en, src='en', dest='ru')
    names_ru = [trans.text for trans in result]
    return names_ru


if __name__ == '__main__':
    _width = 100
    _hight = 50
    if sys.platform == 'win32':
        os.system('color 71')
        os.system('mode con cols=%d lines=%d' % (_width, _hight))
    cur_script = __file__
    PATH_SCRIPT = os.path.abspath(os.path.dirname(cur_script))
    os.chdir(PATH_SCRIPT)
    cc.clearConsol()

    __author__ = 'master by Vint'
    __title__ = '--- Renamer ---'
    __version__ = '0.0.1'
    __copyright__ = 'Copyright 2020 (c)  bitbucket.org/Vintets'
    auth_sh.authorship(__author__, __title__, __version__, __copyright__, width=_width)

    main()

    if not CLOSECONSOLE:
        input('\n---------------   END   ---------------')
    else:
        time.sleep(2)
        exit()

# ==================================================================================================
